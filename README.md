# Демоверсия задания полуфинала олимпиады "Я - профессионал" 2022-2023 по робототехнике
[![Telegram](https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white)](https://t.me/iprofirobots)    [![Yandex IProfi](https://img.shields.io/badge/yandex-%23FF0000.svg?&style=for-the-badge&logo=yandex&logoColor=white)](https://yandex.ru/profi/profile/?page=contests)  [![Mail](https://custom-icon-badges.demolab.com/badge/-iprofi.robotics@yandex.ru-red?style=for-the-badge&logo=mention&logoColor=white)](mailto:iprofi.robotics@yandex.ru)

---
![scene pic](docs/figures/scene_view.jpg)

---

Репозиторий содержит ROS-пакет с минимальным *решением* задачи. Участнику следует, модифицируя этот пакет, решить задачу.

## Задача

Сцена включает в себя автономный багги, способный перемещаться в ограниченном стенами пространстве - складское помещение (далее: склад). Внутри склада разбросаны шары.

Вам необходимо с использованием доступных сенсоров реализовать алгоритм управления автономным багги, который позволит приехать от красного угла до синего избегая столкновений с шарами-препятствиями. 

## Как все работает

Доступны два docker-образа:

- `scene_bachelor_demo` - read-only образ, включающий сцену и робота в gazebo. Образ скачивается из регистра gitlab.
- `problem-bachelor-demo-img` - образ с зависимостями для решения задачи. Образ собирается у вас на компьютере.

Для запуска docker-контейнеров используется инструмент docker-compose. Описание параметров запуска доступно в этом репозитории в файлах:

- `docker-compose.yml ` - если у вас **нет** видеокарты *Nvidia*.
- `docker-compose.nvidia.yml `. - если у вас есть видеокарта от *Nvidia*.

## Установка и настройка окружения
Для настройки окружения необходимо иметь одну из перечисленных операционных систем:
1. Ubuntu 16.04 и старше
2. Windows 10 и старше, с установленным WSL (Не рекомендуется).  

Для подготовки окружения необходимо сделать следующее:
1. Установить docker-engine: [Docker Engine](https://docs.docker.com/engine/install/ubuntu/).  
2. Также необходимо установить docker-compose-plugin: [Docker Compose](https://docs.docker.com/compose/install/linux/).  
3. Если вы планируете использовать видеокарту, установите также nviidia-container-toolkit: [Nvidia Container Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html)

## Запуск решения

Склонируйте репозиторий в рабочую директорию:

    git clone https://gitlab.com/beerlab/iprofi2023/demo/bachelor.git
    cd bachelor

Перед запуском на Linux выполните следующую команду:

    xhost +local:docker

Для запуска сцены и этого пакета используйте команду:

    docker compose -f docker-compose.yml up

В случае необходимости пересборки используйте флаг `--build`:

    docker compose -f docker-compose.yml up --build

Для получения последней версии сцены(обновления), используейте флаг `--pull always`:

    docker compose -f docker-compose.yml up --build --pull always

---
Для локальной пересборки используйте файлы, содержащие в названии "local"

    docker compose -f docker-compose.local.yml up --build

Или

    docker compose -f docker-compose.nvidia.local.yml up --build
---

В файле `docker-compose.yml` хранится описание параметров запуска сцены и решения. По умолчанию запускается `example_node`

    rosrun bachelor example_node

Для открытия новой bash-сессии используйте команду

    docker exec -it problem_bachelor_demo bash

## Быстрый перезапуск решения
Для быстрого изменения и пересборки отредактируйте необходимые файлы и в соседней вкладке перезапустите сервис решения с помощью команды:

    docker compose restart problem


## Оценка

Оценивается время за которое багги доехал из красной зоны в синюю. Колличество баллов равно отрицательному колличеству затраченных секунд. За каждое столкновение начисляется штраф: -10 баллов. Маскимальное время выполнения задания: 5 минут.

