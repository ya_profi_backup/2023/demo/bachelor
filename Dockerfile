ARG BASE_IMG

FROM ${BASE_IMG}

SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y ros-noetic-moveit ros-noetic-kdl-parser ros-noetic-kdl-parser-py

RUN cd src && git clone -b noetic-devel https://github.com/RobotnikAutomation/rbvogui_common.git\ 
    && git clone https://github.com/be2rlab/robotnik_sensors.git

RUN cd /ros_ws && catkin build
ENTRYPOINT [ "/bin/bash", "-ci", "catkin build && source devel/setup.bash && roslaunch bachelor start.launch" ]